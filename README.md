# CLUSTERING (MGE) PIPELINE BY JESSE KERKVLIET
This is a pipeline designed by Jesse Kerkvliet for the clustering of predicted MGE-containing (plasmids, phages and insertion-sequences) contigs present in a dataset using the output obtained from [MetaMobilePicker](https://metamobilepicker.nl).


## INSTALLING

```
git clone https://gitlab.com/mmb-umcu/clustering_pipeline_jk.git

cd clustering_pipeline_JK

conda env create -n clust_pipe_JK -f MMP_clustering.yml
```

## SETTING UP

*Before starting:* The pipeline relies on files having MetaMobilePicker filenames format, so if you're trying to run with non-MetaMobilePicker output files, please make sure you rename your files accordingly (use the provided test files as inspiration).

### 1. Copying the cleaned reads
Copy the trimmed and decontaminated fastq files per sample from the `<sample_name>_processed_reads` MMP output folder to their own folder in `data/00_fastq/` in this repository. For example, readfiles for B100_MET should be in the directory `data/00_fastq/B100_MET/` and for B90_MET in `data/00_fastq/B90_MET/`.

Example directory structure of the test files included in this repository:
```
data
└── 00_fastq
   ├── B90_MET
   │  ├── B90_MET_QC_R1.fastq.gz
   │  └── B90_MET_QC_R2.fastq.gz
   └── B100_MET
      ├── B100_MET_QC_R1.fastq.gz
      └── B100_MET_QC_R2.fastq.gz
```

**The test files and folders are only intended for testing the pipeline and should be removed when running your own samples!**

### 2. Copying the MGE FASTA files
Copy the FASTA files containing the MGEs for each sample from the MMP output `<sample_name>_MetaMobilePicker.fasta` located in `<sample_name>_MGEs` to the `results/02_fasta/` directory in this repository.

Example directory structure of the test files included in this repository:
```
results
├── 00_done
├── 02_fasta
│  ├── B90_MET_MetaMobilePicker.fasta
│  └── B100_MET_MetaMobilePicker.fasta
├── 03_fasta_combined
├── 04_DB
├── 05_clusters
├── 06_tsv
├── 07_mapping
└── 08_ANVIO
```

### 3. Editing the config file
Edit the `clustering_pipeline_jk/src/config.yaml` file. Make sure to follow the example format already in the file (the number of spaces before each line is important!):
```
samples:
    B100_MET:
        MMP: ../results/B100_MET
        reads: ../data/00_fastq/B100_MET
    B90_MET:
        MMP: ../results/B90_MET
        reads: ../data/00_fastq/B90_MET
```


## RUNNING

* First, open a new screen session and get into an interactive session (on the HPC):
```
screen -S clustering

# This is the node where Snakemake will be submitting its jobs from. The jobs themselves will not run here. You might have to adjust the runtime depending on how long the entire pipeline will take.
srun --time=12:00:00 --mem-per-cpu 20G -c 8 --pty bash
```

* Activate the conda environment:
```
conda activate clust_pipe_JK
```

* Now, make sure you're located in the 'src' directory:
```
cd clustering_pipeline_JK/src
```

* Run the pipeline:
```
bash runClustering_slurm.sh
```

## Get the presence absence from the pipeline output

The main output is the Anvio database located in `results/08_ANVIO/MERGED_clust_bowtie_test_cov0_c90/PROFILE.db`.
For the presence absence, if the value is < 0.9, it will be 0. Otherwise, it will be 1.

```
if (!requireNamespace("DBI", quietly = TRUE)) install.packages("DBI")
if (!requireNamespace("RSQLite", quietly = TRUE)) install.packages("RSQLite")
if (!requireNamespace("tidyverse", quietly = TRUE)) install.packages("tidyverse")

library(DBI)
library(RSQLite)
library(tidyverse)

anviodb <- DBI::dbConnect(RSQLite::SQLite(),
                          "results/08_ANVIO/MERGED_clust_bowtie_test_cov0_c90/PROFILE.db")

# 1. Extract the detection_contigs table from the Anvio database
# 2. Create a new column from the "value" column. If the value is < 0.9 make it a 0, otherwise make it a 1
detection_contigs <- tbl(anviodb, "detection_contigs") %>%
  select(item, layer, value) %>%
  mutate(pres_abs = ifelse(value < 0.9, 0, 1)) %>%
  rename(contig = item, sample = layer) %>%
  as_tibble()

DBI::dbDisconnect(anviodb)

write.csv(detection_contigs, "contig_prensence_absence.csv", row.names = FALSE)
```
