###
# MGE clustering workflow
# Author: Jesse Kerkvliet
###

#configfile:"full_broiler_config.yaml"
configfile:"clustering_config.yaml"
# Global variable experiment name
EXPERIMENT = "bowtie_test_cov0_c90"


#def getorigin(sample):
#    return config["samples"][sample]["type"]



#for x in config["samples"]:
##    print("DEBUG----")
#    print(config["samples"][x]["reads"])
#    print("---DEBUG")

rule all:
    input:
        #expand("../results/02_fasta/{sample}_MGEs.fasta",sample=config["samples"]),
        #f"../results/05_clusters/{EXPERIMENT}_clusters_2kb_3plus.fasta",
        #f"../results/05_clusters/{EXPERIMENT}_clusters_2kb.fasta",
        #f"../results/06_tsv/{EXPERIMENT}_clusters_reportfile.tsv",
        #f"../results/06_tsv/{EXPERIMENT}_clusters_2kb_reportfile.tsv",
        #expand("../results/02_fasta/{sample}_1000bp_tag.fasta",sample=config["samples"]),
        #expand(f"../results/07_mapping/{{sample}}_to_{EXPERIMENT}_sorted.bam",sample=config["samples"]),
        #expand(f"../results/done/{EXPERIMENT}{{sample}}_indexed.txt", sample=SAMPLES),
        #expand(f"../results/done/{EXPERIMENT}_hmm_done.txt"),
        #expand(f"../results/done/{EXPERIMENT}_cogs_done.txt"),
        #expand(f"../results/done/{EXPERIMENT}_{{sample}}_profile_done.txt", sample=SAMPLES),
        #f"../results/06_tsv/{EXPERIMENT}_reps.txt",
        f"../results/00_done/{EXPERIMENT}_merge_clust_done.txt",
        #expand(f"../results/00_done/{{sample}}_{EXPERIMENT}_profile_done.txt",sample=config["samples"]),
        #"../results/done/merge_done.txt",
        #f"../results/done/{EXPERIMENT}_linclust.done",
        #expand(f"../results/done/{EXPERIMENT}_merge_clust_done.txt"),
        #expand(f"../results/fasta/{EXPERIMENT}_combined_2kb.fasta"),
        #f"../results/tsv/{EXPERIMENT}_clusters_2kb.tsv",
        #expand("../results/fasta/{sample}_MGEs.fasta",sample=SAMPLES),
        #expand("../results/done/{sample}_extraction.done",sample=SAMPLES)

rule dryrun:
    output:
        f"../results/02_fasta/{EXPERIMENT}/{{sample}}_MGEs.fasta",
    shell:
        """
        touch {output}
        """

# Legacy rule that has been implemented into MMP since v0.7
#rule extract_MGEs:
#    input:
#        mmp_output="../results/01_mmp_output/{sample}_MetaMobilePicker.out",
#        assembly="../results/02_fasta/{sample}_1000bp.fasta",
#    output:
#        fasta="../results/02_fasta/{sample}_MGEs.fasta",
#        done="../results/00_done/{sample}_extraction.done",
#    shell:
#        """
#        python extractSequences.py {input.mmp_output} {input.assembly} {output.fasta} full
#        touch {output.done}
#        """

# Tag all MGEs with sample name
rule add_samplename:
    input:
        fasta="../results/02_fasta/{sample}_MetaMobilePicker.fasta"
    output:
        fasta="../results/02_fasta/{sample}_1000bp_tag.fasta"
    params:
        sample="{sample}"
    shell:
        """
        cat {input.fasta} | sed '/^>/ s/$/_{params.sample}/g' > {output.fasta}
        """
       
# Take all fasta files and put them in one file (input for clustering)
rule combine_fasta:
    input:
        [
            "../results/02_fasta/{sample}_1000bp_tag.fasta".format(sample=sample)
            for sample in config["samples"]
        ],
    output:
        fasta=f"../results/03_fasta_combined/{EXPERIMENT}_combined.fasta",
    shell:
        """
        echo {input}
        cat {input} >> {output.fasta}
        """

# RULE CURRENTLY NOT IN USE
rule filterARGS:
    input:
        fasta=f"../results/03_fasta_combined/{EXPERIMENT}_combined.fasta",
    output:
        fasta=f"../results/03_fasta_combined/{EXPERIMENT}_ARGs_combined.fasta"
    shell:
       """
       touch {output.fasta}
       """
# Remove sequences shorter than 2kb
rule filterLength:
    input:
        f"../results/03_fasta_combined/{EXPERIMENT}_combined.fasta",
    output:
        f"../results/03_fasta_combined/{EXPERIMENT}_combined_2kb.fasta",
    shell:
        """
        seqtk seq -L 2000 {input} > {output}
        #anvi-script-reformat-fasta {input} -o {output} -l 2000
        """

# Create MMSeqsDB for clustering
rule make_MMSeqsDB:
    input:
        fasta=f"../results/03_fasta_combined/{EXPERIMENT}_combined_2kb.fasta",
    params:
        db=f"../results/04_DB/{EXPERIMENT}_2kb",
    output:
        done=f"../results/00_done/{EXPERIMENT}_mmseqs_db.done",
    shell:
        """
        mmseqs createdb {input.fasta} {params.db}
        touch {output.done}
        """

# linear clustering algorithm MMSeqs
# Covmode 0 clusters on both length and identity
rule lincluster:
    input:
        done=f"../results/00_done/{EXPERIMENT}_mmseqs_db.done",
    output:
        done=f"../results/00_done/{EXPERIMENT}_linclust.done",
    params:
        db=f"../results/04_DB/{EXPERIMENT}_2kb",
        name=f"../results/04_DB/{EXPERIMENT}_clusters_2kb",
        covmode="0",
        covperc="0.90"
    threads: 32
    resources:
        mem=128,
    shell:
        """
        mmseqs cluster {params.db} {params.name} tmp --cov-mode {params.covmode} -c {params.covperc} --threads {threads} --split-memory-limit 64G --min-seq-id 0.90
        touch {output.done} 
        """


# ../results/DB/EFFORT_MGEs_combined_clust
# After clustering, subDB is created with the clusters
rule createsubdb:
    input:
        f"../results/00_done/{EXPERIMENT}_linclust.done",
    output:
        done=f"../results/00_done/{EXPERIMENT}_subdb.done",
    params:
        indb=f"../results/04_DB/{EXPERIMENT}_2kb",
        outdb=f"../results/04_DB/{EXPERIMENT}_clusters_2kb",
        reps=f"../results/04_DB/{EXPERIMENT}_clusters_2kb_rep",
    shell:
        """
        mmseqs createsubdb {params.outdb} {params.indb} {params.reps}
        touch {output.done}
        """

# Get fasta file with cluster representatives
rule extractClusterFasta:
    input:
        f"../results/00_done/{EXPERIMENT}_subdb.done",
    output:
        fasta=f"../results/05_clusters/{EXPERIMENT}_clusters_2kb_rep.fasta",
    params:
        indb=f"../results/04_DB/{EXPERIMENT}_clusters_2kb_rep",
    shell:
        """
        mmseqs convert2fasta {params.indb} {output}
        """

# Get tabular output of clusters and representatives
rule extractTabularClusters:
    input:
        f"../results/00_done/{EXPERIMENT}_subdb.done",
    output:
        f"../results/06_tsv/{EXPERIMENT}_clusters_2kb.tsv",
    params:
        indb=f"../results/04_DB/{EXPERIMENT}_2kb",
        subdb=f"../results/04_DB/{EXPERIMENT}_clusters_2kb",
    shell:
        """
        mmseqs createtsv {params.indb} {params.indb} {params.subdb} {output}
        """

# Only keep ARGs (commented out code to filter on singletons)
rule filterClusters:
    input:
        tsv=f"../results/06_tsv/{EXPERIMENT}_clusters_2kb.tsv",
        fasta=f"../results/05_clusters/{EXPERIMENT}_clusters_2kb_rep.fasta",
    output:
        fasta=f"../results/05_clusters/{EXPERIMENT}_clusters_2kb_ARGs.fasta",
        reps=f"../results/06_tsv/{EXPERIMENT}_reps.txt",
    conda:
        "envs/fasr.yml"
    shell:
        """
        grep "resfinder" {input.tsv} | sort | uniq > {output.reps}
        #cut -f 1 {input.tsv} | sort | uniq -c | awk '{{if($1 > 2){{print $0}}}}' | awk '{{$1="";print}}' | sed 's/ //g' > {output.reps}
        
        faSomeRecords {input.fasta} {output.reps} {output.fasta}
        """

# Create bowtie index for alignment
rule index_assembly:
    input:
        f"../results/05_clusters/{EXPERIMENT}_clusters_2kb_simplified.fasta",
    output:
        f"../results/00_done/{EXPERIMENT}_index_done.txt",
    params:
        prefix=f"{EXPERIMENT}"
    conda:
        "envs/fasr.yml"
    shell:
        """
        #bwa index {input}
        bowtie2-build {input} ../results/07_mapping/{params.prefix}
        touch {output}
       
        """

### FIX THIS PART
### SHOULD NOT RUN. BASED ON A FAULTY INPUT FASTQ
rule straighten_reads:
    input:
        fwd = lambda wildcards: config["samples"][wildcards.sample]["reads"]+"/{sample}_QC_R1.fastq.gz",
        rev = lambda wildcards: config["samples"][wildcards.sample]["reads"]+"/{sample}_QC_R2.fastq.gz",
    output:
        fwd = "../results/00_fastq/{sample}/{sample}_QC_R1.fastq.gz",
        rev = "../results/00_fastq/{sample}/{sample}_QC_R2.fastq.gz",
        se = "../results/00_fastq/{sample}/{sample}_QC_SE.fastq.gz"
    threads: 32
    params: 
        sample="{wildcards.sample}"
    shell:
        """
        repair.sh -Xmx16g in1={input.fwd} in2={input.rev} out1={output.fwd} out2={output.rev} outs={output.se} repair
        """

# Mapping using bowtie2 (commented out earlier trial with bwa, little difference)
rule read_recruitment:
    input:
        fwd= lambda wildcards: config["samples"][wildcards.sample]["reads"]+"/{sample}_QC_R1.fastq.gz",
        rev= lambda wildcards: config["samples"][wildcards.sample]["reads"]+"/{sample}_QC_R2.fastq.gz",
        #fwd = "../results/00_fastq/{sample}/{sample}_QC_R1.fastq.gz",
        #rev = "../results/00_fastq/{sample}/{sample}_QC_R2.fastq.gz",
        #se =  "../results/00_fastq/{sample}/{sample}_QC_SE.fastq.gz",
   
        #fwd=expand("{reads}/{sample}_QC_R1.fastq.gz",reads=config[sample]["reads"],sample=sample),
        #rev=expand("{reads}/{sample}_QC_R2.fastq.gz",reads=config[sample]["reads"],sample=sample), 
        indexed=f"../results/00_done/{EXPERIMENT}_index_done.txt",
        assembly=f"../results/05_clusters/{EXPERIMENT}_clusters_2kb_simplified.fasta",
    output:
        bam=temp(f"../results/07_mapping/{{sample}}_to_{EXPERIMENT}.bam"),
    conda:
        "envs/fasr.yml"
    params:
        prefix=f"{EXPERIMENT}"
    threads: 32
    shell:
        """
        #bwa mem {input.assembly} {input.fwd} {input.rev} -t {threads} | samtools view -bS -F 4 -o {output}
        #bowtie2 -p
        bowtie2 -p {threads} -q -x ../results/07_mapping/{params.prefix} -1 {input.fwd} -2 {input.rev} | samtools view -bS -F 4 -o {output}

        """

# Samtools sort the bams
# Candidate to be removed after running: the unsorted BAM
rule sort_bams:
    input:
        f"../results/07_mapping/{{sample}}_to_{EXPERIMENT}.bam",
    output:
        f"../results/07_mapping/{{sample}}_to_{EXPERIMENT}_sorted.bam",
    shell:
        """
        samtools sort {input} -o {output}
        """

# Create samtools index for alignment
rule index_bams:
    input:
        f"../results/07_mapping/{{sample}}_to_{EXPERIMENT}_sorted.bam",
    output:
        f"../results/00_done/{EXPERIMENT}_{{sample}}_indexed.txt",
    shell:
        """
        samtools index {input}
        touch {output}
        """

#####
### ANVI'O PART STARTS HERE
#####

# Renames all fasta sequences to remove 'illegal' characters. 
# Report file is saved to backtrace this process.
rule simplify_fasta:
    input:
        f"../results/05_clusters/{EXPERIMENT}_clusters_2kb_ARGs.fasta",
        #f"../results/05_clusters/{EXPERIMENT}_clusters_2kb_rep.fasta",

    output:
        #f"../results/05_clusters/{EXPERIMENT}_clusters_2kb_3plus_simplified.fasta",
        f"../results/05_clusters/{EXPERIMENT}_clusters_2kb_simplified.fasta",
        f"../results/06_tsv/{EXPERIMENT}_clusters_2kb_reportfile.tsv"
    singularity:
        "docker://meren/anvio:7.1_main_0522"
    shell:
        """
        anvi-script-reformat-fasta {input} -o {output[0]} --report-file {output[1]} -l 0 --simplify-names
        """

# Create ContigsDB for the clustering fasta
rule make_contigsdb:
    input:
        f"../results/05_clusters/{EXPERIMENT}_clusters_2kb_simplified.fasta",
    output:
        f"../results/08_ANVIO/{EXPERIMENT}_CONTIGS-EFFORT-clusters.db",
    threads: 32
    resources:
        runtime=300,
        mem=64,
    singularity:
        "docker://meren/anvio:7.1_main_0522"
    shell:
        """
        anvi-gen-contigs-database -f {input} \
                          -o {output} \
                          -n "mmseqs clusters of the MGEs of 20 EFFORT samples (TEST)" \
                          --num-threads {threads}
        """

# Initialize the ContigsDB with HMM
rule anvi_hmms:
    input:
        f"../results/08_ANVIO/{EXPERIMENT}_CONTIGS-EFFORT-clusters.db",
    output:
        f"../results/00_done/{EXPERIMENT}_hmm_done.txt",
    threads: 32
    resources:
        mem=64,
        runtime=360,
    singularity:
        "docker://meren/anvio:7.1_main_0522"
    shell:
        """
        anvi-run-hmms -c {input} --num-threads {threads} --just-do-it
        touch {output}
        """

# Initialize the ContigsDB with COGS
rule anvi_cogs:
    input:
        f"../results/08_ANVIO/{EXPERIMENT}_CONTIGS-EFFORT-clusters.db",
    output:
        f"../results/00_done/{EXPERIMENT}_cogs_done.txt",
    threads: 32
    resources:
        runtime=360,
        mem=32,
    singularity:
        "docker://meren/anvio:7.1_main_0522"
    shell:
        """
        anvi-run-ncbi-cogs -c {input} --num-threads 16
        touch {output}
        """

# Create a ProfileDB (sample specific information) in Anvi'o per sample
# After the entire pipeline has run, this can probably be removed (you create one big one in the next step)
rule profileDB:
    input:
        indexdone=ancient(f"../results/00_done/{EXPERIMENT}_{{sample}}_indexed.txt"),
        contigdb=ancient(f"../results/08_ANVIO/{EXPERIMENT}_CONTIGS-EFFORT-clusters.db"),
        bam=f"../results/07_mapping/{{sample}}_to_{EXPERIMENT}_sorted.bam",
    output:
        ancient(f"../results/00_done/{{sample}}_{EXPERIMENT}_profile_done.txt"),
    params:
        outdir=f"../results/08_ANVIO/{{sample}}_{EXPERIMENT}_profile",
        sample="{sample}"
    threads: 16
    resources:
        runtime=300,
        mem=64,
    singularity:
        "docker://meren/anvio:7.1_main_0522"
    shell:
        """
        
        anvi-profile -c {input.contigdb} -i {input.bam} --num-threads {threads} -o {params.outdir} --sample-name {params.sample}
        touch {output}
        """

# Merge all ProfileDBs into one so you can load it into Anvi'o
rule mergeProfile:
    input:
        ancient(
            [
                f"../results/00_done/{sample}_{EXPERIMENT}_profile_done.txt"
                for sample in config["samples"]
            ]
        ),
    output:
        f"../results/00_done/{EXPERIMENT}_merge_clust_done.txt",
    params:
        contigdb=f"../results/08_ANVIO/{EXPERIMENT}_CONTIGS-EFFORT-clusters.db",
        exp=f"{EXPERIMENT}",
    singularity:
        "docker://meren/anvio:7.1_main_0522"
    resources:
        mem=200
    shell:
        """
        anvi-merge ../results/08_ANVIO/*{params[1]}_profile/PROFILE.db -c {params.contigdb} -o ../results/08_ANVIO/MERGED_clust_{params.exp} --force-overwrite
        touch {output}
        """


