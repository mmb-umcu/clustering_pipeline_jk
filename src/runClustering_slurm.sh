#!/bin/bash

# Snakemake requires the full path of the profile if it is not in a default location
profile=$(realpath -e ../slurm_smk7)

snakemake -s clustering_pipeline.smk \
    --use-conda \
    --use-singularity \
    --profile ${profile}  \
    --singularity-args  '--home $PWD/.. --bind $TMPDIR,$PWD/..' \
    --configfile config.yaml \
    --cores 32 \
    --latency-wait 60
